﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quartz;
using SaonGroup.JobApp.Models;

namespace SaonGroup.JobApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IScheduler _scheduler;

        public HomeController(ILogger<HomeController> logger, IScheduler scheduler)
        {
            _logger = logger;
            _scheduler = scheduler;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> StartCheckExpiration()
        {
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity($"CheckExpirationJob -{ DateTime.Now}")
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(5).RepeatForever())
                .Build();

            IJobDetail job = JobBuilder.Create<JobContext>().WithIdentity("Check Expiration Date").Build();

            await _scheduler.ScheduleJob(job, trigger);
            return RedirectToAction("Index");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
