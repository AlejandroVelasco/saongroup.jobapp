﻿using Quartz;
using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    namespace SaonGroup.JobApp.Models
    {
        public class Job: IDateCreatedAndUpdated
        {
            [Key]
            public int JobId { get; set; }

            [Required(ErrorMessage = "Job name is required")]
            [DisplayName("Name")]
            [Column(TypeName = "nvarchar(250)")]
            public string JobTitle { get; set; }

            [Column(TypeName = "nvarchar(1000)")]
            [DisplayName("Description")]
            public string Description { get; set; }

            [DisplayName("Expiration date")]
            [CurrentDate(ErrorMessage = "Date must be after or equal to current date")]
            public DateTime ExpiresAt { get; set; }

            [DisplayName("State")]
            public Boolean State { get; set; } = true;

            //THIS FIELDS ARE USE TO REGISTER THE CREATION DATE AND UPDATE DATE OF EACH RECORD. 
            //THESE FIELDS ARE DESIGNED TO BE SETTED AUTOMATICALLY.
            //ALSO CAN BE USED BY ANY OTHER MODEL.
            public DateTime CreatedAt { get; set; }
            public DateTime? UpdatedAt { get; set; }
        }
    }
