﻿using Microsoft.EntityFrameworkCore;
using Quartz;
using Quartz.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SaonGroup.JobApp.Models
{
    public class JobContext : DbContext, IJob
    {
        public JobContext(DbContextOptions<JobContext> options) : base(options)
        {

        }

        public DbSet<Job> Jobs { get; set; }

        public async Task Execute(IJobExecutionContext context)
        {
            foreach (var item in Jobs)
            {
                if(item.ExpiresAt <= DateTime.Now){
                    if (item.State)
                    {
                        item.State = false;
                    }
                }
            }
            base.SaveChanges();
        }

        
        //Overriding SaveChanges function in order to set Create or update Datetime.
        public override int SaveChanges()
        {
            SetProperties();
            return base.SaveChanges();

        }

        //Overriding SaveChangesAsync function in order to set Create or update Datetime.

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetProperties();
            return base.SaveChangesAsync(cancellationToken);
        }

        //Function Used to Set creation date in case of adding a new record or updating date in case of modifying a record.
        private void SetProperties()
        {
            foreach (var entity in ChangeTracker.Entries().Where(p => p.State == EntityState.Added))
            {
                var created = entity.Entity as IDateCreated;
                if (created != null)
                {
                    created.CreatedAt = DateTime.Now;
                }
            }

            foreach (var entity in ChangeTracker.Entries().Where(p => p.State == EntityState.Modified))
            {
                var modified = entity.Entity as IDateUpdated;
                if (modified != null)
                {
                    modified.UpdatedAt = DateTime.Now;
                }
            }
        }
    }
}