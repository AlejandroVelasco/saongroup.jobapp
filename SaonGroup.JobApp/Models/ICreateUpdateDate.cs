﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaonGroup.JobApp.Models
{
    //Created and Update Interfaces
    public interface IDateCreated
    {
        DateTime CreatedAt { get; set; }
    }
    public interface IDateUpdated
    {
        DateTime? UpdatedAt { get; set; }
    }

    public interface IDateCreatedAndUpdated : IDateCreated, IDateUpdated
    {

    }
}
